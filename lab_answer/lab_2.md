1. Apakah perbedaan antara JSON dan XML?
    Perbedaan utama antara JSON dan XML dapat dilihat dari segi tipe.  JSON adalah bahasa meta yang bernotasi objek JavaScript. data. XML adalah bahasa markup yang berformat independen antara software dan hardware. Keduanya sama-sama digunakan dalam pertukaran, namun dari segi kompleksitasnya JSON lebih sederhana dan mudah dibaca dibanding XML yang lebih rumit. Kerumitan tersebut memang disebabkan karena XML yang bersifat extensible sehingga kerap ditekankan untuk penguraian yang lebih luas daripada JSON.

    Perbedaan-perbedaan lainnya dapat diamati dari orientasi, array, dan tentu saja ekstensi masing-masing file. Sebuah file JSON beroientasi pada data sedangkan XML berorientasikan pada dokumen. Selain itu, JSON mendukung penggunaan format array yang sayangnya XML tidak. Terakhir, ekstensi file JSON diakhiri dengan ekstensi .json, berbeda dengan XML yang diakhiri dengan ekstensi file .xml.

    referensi :
    https://id.sawakinome.com/articles/technology/difference-between-json-and-xml-2.html 
    https://www.monitorteknologi.com/perbedaan-json-dan-xml/  


2. Apakah perbedaan antara HTML dan XML?
    HTML dan XML pertama kali dapat dibedakan melalui definisinya. HTML hanyalah sebuah bahasa mark up standar yang ditunjukan untuk membuat suatu halaman web. Di lain pihak, XML adalah bahasa mark up yang mengandung seperangkat aturan yang berfungsi untuk menyandikan dokumen ke dalam format yang dapat dibaca oleh manusia dan mesin. Oleh karena itu, XML digunakan oleh pada developer untuk menyimpan dan mengirimkan data sedangkan HTML hanya dapat digunakan untuk menampilkan data.

    Kedua bahasa mark up di atas sama-sama memiliki tag. Hal yang membedakan aturan tag di antara keduanya adalah pada bahasa XML tag yang dipakai bebas sesuai yang kita buat, namun hal tersebut tidak berlaku pada HTML yang diharuskan untuk menggunakan tag-tag tertentu untuk menjalankan fungsi yang ada. Selanjutnya, XML dikenal case sensitive yang dapat membedakan huruf kapital dengan huruf kecil, tidak dengan HTML yang cenderung case insensitive. Kemudian, XML di dalamnya terdiri atas data struktural yang dimana HTML tidak memilikinya. Perbedaan terakhir yang dapat diamati dari keduanya adalah dari segi tag penutup. XML mewajibkan developer untuk menyajikan tag penutup, berbeda dengan HTML yang tidak selalu mewajibkan pemakaian tag penutup.

    Referensi :
    https://www.niagahoster.co.id/blog/xml/ 
    https://perbedaan.budisma.net/perbedaan-html-dan-xml.html 